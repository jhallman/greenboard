# gets current working directory for better portability
# when working with absolute paths
import os

base_path = os.getcwd()
def get_path():
	return base_path

## fallback module if getcwd fails
# import getpass
# user = getpass.getuser()
# if user == 'Jhallman':
# 	base_path = '/Users/Jhallman/Sites/'
# if user == 'jhh':
# 	base_path = '/home/jhh/Sites/teaplusplus/'
