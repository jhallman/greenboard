from django.db import models
from formatChecker import ContentTypeRestrictedFileField

# flier model
class Flier(models.Model):
	#name of uploading user, possibly changed to username
	name = models.CharField(max_length=50)
	email = models.EmailField()

	#date uploaded
	submission_date = models.DateField()
	
	#final date for flier to appear/ expiration date
	#optional field? would have to set a default value
	#end_date = models.DateField(blank=True)

	#image file itself, must be of accepted file types, current support only
	#for .jpg and .png
	image = ContentTypeRestrictedFileField(upload_to='flier_images',
		content_types=['image/jpeg', 'image/png'],
		max_upload_size=2621440)

	def __unicode__(self):
		return u'%s, %s' % (self.name, self.image)
