from django import forms
from models import Flier

# class Flier_Upload_Form(forms.Form):
# 	name = forms.CharField()
# 	email = forms.EmailField()
# 	#end_date = forms.DateField()
# 	#flier field must be either jpg or png
# 	image = ContentTypeRestrictedFileField(upload_to='flier_images',
# 		content_types=['image/jpeg', 'image/png'],
# 		max_upload_size=2621440)


class FlierForm(forms.ModelForm):
	class Meta:
		model = Flier
		fields = ('name', 'email', 'image')
