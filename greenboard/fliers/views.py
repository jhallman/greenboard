from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404
from fliers.forms import FlierForm
from fliers.models import Flier
import datetime


#displays flier page 
def show_flier(request, location):
	if location == '1':
		images = Flier.objects.order_by('-image')[0:2]
	if location == '2':
		images = Flier.objects.order_by('image')[0:2]
	#else:
		#raise Http404()
	image1 = images[0].image
	image2 = images[1].image
	return render_to_response('flier.html', {'image1': image1, 'image2': image2})

#handles flier upload page
def upload_flier(request):
	if request.method == 'POST':
		form = FlierForm(request.POST, request.FILES)
		if form.is_valid():
			cd = form.cleaned_data
			#process data
			handle_flier_upload(cd['name'], cd['email'], cd['image'])
			return HttpResponseRedirect('/upload/thanks')
	else:
		form = FlierForm()
	return render_to_response('upload_form.html', {'form': form})

#basic thank you page upon successful upload, possibly link to flier view
def upload_thanks(request):
	return render_to_response('thanks.html')


# helper functions
def handle_flier_upload(name, email, image):
	now = datetime.datetime.now()
	f = Flier(name=name, email=email, submission_date=now, image=image)
	f.save()

	