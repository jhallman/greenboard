GREENBOARD

Goal:
To create a web-based solution to posting fliers and related content to electronic corkboards (televison screens?) to be located in various public spaces. 

Key Features:

Users should be able to access the site and upload a file (pdf? .doc? html? jpeg?), along with some basic information, which would then be displayed on the corkboard. 

There would necessarily be some sort of vetting to ensure fliers are relevant/appropriate.  This could be multi-layered.  And application-wide feature could form the first layer of filtration, while a sort of administrative view could allow the site manager to ultimately control the content displayed.

Screen real estate will have to be divided between fliers. Practical limitations mean the screen would be smaller than most corkboards.  It is possible that some sort of rotation sequence could increase the available space.

The application would be comprised of two main views.  One would simply display fliers (to be used on the public corkboards).  The other would be accessible from personal computers, and possibly allow for more detailed information, links, and contact opportunities (email, social networking, etc).

TODO:

1) Validation for flier uploads - only jpg and png. current check only detects file type, need to further ensure that files are actually images.

2) File conversion module for transforming pdf and doc to png

3) view function to determine which fliers are displayed at what time, perhaps an area specific url/page?

4) authentication for users? need to prevent malicious uploads.  Seems that a human approval system is going to be necessary.  Even with safeguards, there is no way to ensure the images uploaded are appropriate.

5) admin interface for business owners.  they need to be able to edit the content on their page if they wish. 

6) location specific flier pages --> change number to strings, fix hardcoding of locations in view

7) MD5 hash? Append id?  Need to make filenames unique.

8) resize images on upload?


CHANGELOG

05/27/12 11:13 AM 
modified show_flier view so as to display different fliers for different locations/urls
directory is currently just a 1 digit number, 1 and 2 (both hardcoded into the view)

05/28/12 7:13 AM
attempted to implement file type/size validation, was not successful. reverted to previous commit.

05/28/12 8:00 AM
implemented file type/size validation. achieved by changing form to ModelForm helper class

05/29/12 6:30 AM
minor adjustments to urls.py, imported entire view module instead of individual functions

06/4/12 10:25 AM
added filepath module for better portability when absolute paths are required




