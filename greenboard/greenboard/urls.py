from django.conf.urls import patterns, include, url
from fliers import views
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'greenboard.views.home', name='home'),
    # url(r'^greenboard/', include('greenboard.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    #flier display pages
    (r'^fliers/(?P<location>\d{1})/$', views.show_flier),
    (r'^upload/$', views.upload_flier),
    (r'^upload/thanks/$', views.upload_thanks),


    
    #ugh i need to read up on these...
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
